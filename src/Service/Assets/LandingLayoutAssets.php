<?php

namespace App\Service\Assets;

use App\Service\Assets\AbstractAssets;

class LandingLayoutAssets extends AbstractAssets
{
    public static function getHomeAssets(): array
    {
        $package = self::getPackage();
        $landingPageAssets = self::getBaseImages($package);

        return array_merge($landingPageAssets, [
            // 'head_decor' => $package->getUrl('/images/top_decor.svg'),
        ]);
    }
}