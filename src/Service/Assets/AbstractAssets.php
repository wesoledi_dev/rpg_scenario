<?php

namespace App\Service\Assets;

use Symfony\Component\Asset\Package;
use Symfony\Component\Asset\VersionStrategy\EmptyVersionStrategy;

abstract class AbstractAssets
{
    protected static function getPackage(): Package
    {
        return new Package(new EmptyVersionStrategy());
    }

    protected static function getBaseImages(Package $package): array
    {
        $package = $package ?? self::getPackage();
        return array_merge(self::getLogo($package), [
            'social' => self::getSocialIcons($package),
        ]);
    }

    protected static function getLogo(Package $package = null): array
    {
        $package = $package ?? self::getPackage();
        return [
            'logo' => $package->getUrl('/images/logo-color.svg'),
            'logo_mono' => $package->getUrl('/images/logo-mono.svg'),
        ];
    }

    private static function getSocialIcons(Package $package = null): array
    {
        $package = $package ?? self::getPackage();
        return [
            'linkedin' => $package->getUrl('/images/linkedin.svg'),
            'github' => $package->getUrl('/images/github.svg'),
            'internet' => $package->getUrl('/images/world-grid.svg'),
            'facebook' => $package->getUrl('/images/facebook.svg'),
        ];
    }
}