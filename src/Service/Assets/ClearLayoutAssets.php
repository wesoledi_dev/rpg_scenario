<?php

namespace App\Service\Assets;

use App\Service\Assets\AbstractAssets;

class ClearLayoutAssets extends AbstractAssets
{
    public static function getAuthAssets(): array
    {
        $package = self::getPackage();
        $basePageAssets = self::getBaseImages($package);

        return array_merge($basePageAssets, [
            // 'head_decor' => $package->getUrl('/images/top_decor.svg'),
        ]);
    }
}