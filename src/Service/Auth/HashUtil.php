<?php

namespace App\Service\Auth;

class HashUtil
{
    private const DEFAULT_PASSWORD_BONUS_SUFFIX = 'CA';

    public static function hashPassword(string $password): string
    {
        return sha1($password . self::getPasswordBonusSuffix());
    }

    public static function prepareUserUniqueHash(string $email): string
    {
        $randomNum = rand(100000, 999999);
        $stringToHash = sprintf('%s%d', $email, $randomNum);
        return sha1($stringToHash);
    }

    private static function getPasswordBonusSuffix(): string
    {
        return $_ENV['PASSWORD_BONUS_SUFFIX'] ?? self::DEFAULT_PASSWORD_BONUS_SUFFIX;
    }
}