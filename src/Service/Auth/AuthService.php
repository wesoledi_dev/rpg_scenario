<?php

namespace App\Service\Auth;

use Symfony\Component\HttpFoundation\Session\SessionInterface;
use App\Service\Auth\UserDataUtil;
use App\Entity\App\Auth\AuthUserData;
use App\Service\Auth\UserService;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Session\Session;

class AuthService
{
    private const USER_NAME = UserDataUtil::USER_NAME;
    private const USER_HASH = UserDataUtil::USER_HASH;
    private const USER_EMAIL = UserDataUtil::USER_EMAIL;

    private $session;

    public function __construct(SessionInterface $session = null)
    {
        if (empty ($session)) {
            $session = $this->getSession();
        }
        $this->session = $session;
    }

    public function isSigned(): bool
    {
        return !empty($this->session->get(self::USER_HASH)) && !empty($this->session->get(self::USER_NAME));
    }

    public function signIn(string $email, string $password, ManagerRegistry $doctrine): bool
    {        
        if (empty($email) || empty($password)) {
            return false;
        }

        $dbService = new UserService($doctrine);
        $user = $dbService->getByEmailAndPassword($email, $password);

        if (empty($user) || empty($user->getId())) {
            return false;
        }

        $AuthUserData = new AuthUserData($user->getName(), $user->getHash(), $user->getEmail());
        $this->setLocalUserData($AuthUserData);

        return true;
    }

    public function signOut(): void
    {
        $this->session->clear();
    }

    public function getUserData(): ?AuthUserData
    {
        if (!$this->isSigned()) {
            return null;
        }

        return new AuthUserData(
            $this->session->get(self::USER_NAME),
            $this->session->get(self::USER_HASH),
            $this->session->get(self::USER_EMAIL)
        );
    }

    public function register(string $email, string $name, string $password, ManagerRegistry $doctrine): bool
    {
        if (empty($email) || empty($password) || empty($name)) {
            return false;
        }

        $user = null;
        $dbService = new UserService($doctrine);
        $user = $dbService->addUser($email, $name, $password);
        
        if (empty($user)) {
            return false;
        }

        $AuthUserData = new AuthUserData($user->getName(), $user->getHash(), $user->getEmail());
        $this->setLocalUserData($AuthUserData);
        return true;
    }

    private function setLocalUserData(AuthUserData $userData): void
    {
        $this->session->set(self::USER_NAME, $userData->getName());
        $this->session->set(self::USER_HASH, $userData->getHash());
        $this->session->set(self::USER_EMAIL, $userData->getEmail());
    }

    private function getSession(): SessionInterface
    {
        $session = new Session();
        try {
            $session->start();
        } catch (\Exception $e) {}
        return $session;
    }
}