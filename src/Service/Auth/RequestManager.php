<?php

namespace App\Service\Auth;

use App\Service\Auth\AuthService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Router;

class RequestManager
{
    public static function processRequest(Request $request, KernelInterface $kernel): Request
    {
        if (!self::isSecuredLink($request, $kernel->getContainer()->get('router'))) {
            return $request;
        }

        $loginService = new AuthService();
        if ($loginService->isSigned()) {
            return $request;
        }

        return self::redirectToLoginPage();
    }

    private static function redirectToLoginPage(): Request
    {
        return Request::create(
            '/login',
            'GET',
            []
        );
    }

    private static function isSecuredLink(Request $request, Router $router): bool
    {
        $collection = $router->getRouteCollection();
        $allRoutes = $collection->all();

        $requestedUriPath = $request->server->get('REQUEST_URI');

        foreach($allRoutes as $route) {
            if ($route->getPath() === $requestedUriPath) {
                return $route->getDefaults()['needLogin'] ?? false;
            }
        }

        return false;
    }
}