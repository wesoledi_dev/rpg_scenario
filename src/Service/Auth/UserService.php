<?php

namespace App\Service\Auth;

use Doctrine\Persistence\ManagerRegistry;
use App\Entity\User;
use App\Service\Auth\HashUtil;
use App\Exception\Auth\RegisterException;
use App\Service\Auth\AuthService as LoginService;

class UserService
{
    private ManagerRegistry $doctrine;

    public function __construct(ManagerRegistry $doctrine)
    {
        $this->doctrine = $doctrine;
    }

    public static function getOwner(ManagerRegistry $doctrine): User
    {
        $userService = new UserService($doctrine);
        return $userService->getBySession();
    }

    public function getByEmailAndPassword(string $email, string $password): ?User
    {
        return $this->doctrine
            ->getRepository(User::class)
            ->findBy([
                'email' => $email,
                'pass' => HashUtil::hashPassword($password),
            ], ['id' => 'DESC'], 1)[0] ?? null;
    }

    public function getByEmail(string $email): ?User
    {
        return $this->doctrine
            ->getRepository(User::class)
            ->findBy([
                'email' => $email,
            ], ['id' => 'DESC'], 1)[0] ?? null;
    }

    public function getBySession(): ?User
    {
        $loginService = new LoginService();
        $usersData = $loginService->getUserData();

        return $this->doctrine
            ->getRepository(User::class)
            ->findBy([
                'name' => $usersData->getName(),
                'hash' => $usersData->getHash(),
            ], ['id' => 'DESC'], 1)[0] ?? null;
    }

    public function addUser(
        string $email,
        string $name,
        string $password
    ): User {
        if (empty($email) || empty($name) || empty($password)) {
            throw RegisterException::emptyField();
        }

        $userExisting = $this->getByEmail($email);
        if (isset($userExisting) && !empty($userExisting->getId())) {
            throw RegisterException::userExist();
        }

        try {
            $user = new User();
            $user->setEmail($email);
            $user->setName($name);
            $user->setPass(HashUtil::hashPassword($password));
            $user->setHash(HashUtil::prepareUserUniqueHash($email));
            $user->setActive(true);

            $entityManager = $this->doctrine->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            return $user;
        } catch (\Throwable $e) {
            throw RegisterException::saveError();
        }
    }
}