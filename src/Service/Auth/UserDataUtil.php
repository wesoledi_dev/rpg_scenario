<?php

namespace App\Service\Auth;

class UserDataUtil
{
    public const USER_NAME = 'user.name';
    public const USER_HASH = 'user.hash';
    public const USER_EMAIL = 'user.email';

    public static function createUserData(string $name, string $hash, string $email = ''): array
    {
        return [
            self::USER_NAME => $name,
            self::USER_HASH => $hash,
            self::USER_EMAIL => $email,
        ];
    }
}