<?php

namespace App\Exception\Auth;

class RegisterException extends \Exception
{
    public const REGISTER_OK = 1;
    public const REGISTER_EMPTY_DATA = -1;
    public const REGISTER_USER_EXIST = -2;
    public const REGISTER_DATABASE_INSERT = -3;
    public const REGISTER_UNKNOWN = -99;

    public const TEXTS = [
        self::REGISTER_OK => 'Registered',
        self::REGISTER_EMPTY_DATA => 'All fields must be filled',
        self::REGISTER_USER_EXIST => 'User exist',
        self::REGISTER_DATABASE_INSERT => 'Can\'t save',
        self::REGISTER_UNKNOWN => 'Unknow error',
    ];

    public static function emptyField(): self
    {
        return self::createException(self::REGISTER_EMPTY_DATA);
    }

    public static function userExist(): self
    {
        return self::createException(self::REGISTER_USER_EXIST);
    }

    public static function saveError(): self
    {
        return self::createException(self::REGISTER_DATABASE_INSERT);
    }

    private static function createException($code): self
    {
        $exception = new self(self::TEXTS[$code]);
        $exception->registerErrorCode = $code;
        return $exception;
    }
}