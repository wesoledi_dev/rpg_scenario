<?php

namespace App\Entity\App\Auth;

class AuthUserData
{
    private string $name = '';
    private string $hash = '';
    private string $email = '';
    private bool $signedIn = false;

    public function __construct(string $name, string $hash, string $email = '')
    {
        $this->name = $name;
        $this->hash = $hash;
        $this->email = $email;
    }

    public function setSignedInStatus(bool $status): self
    {
        $this->signedIn = $status;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getHash(): string
    {
        return $this->hash;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function isSigned(): bool
    {
        return $this->signedIn;
    }
}