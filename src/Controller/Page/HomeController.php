<?php

namespace App\Controller\Page;

use App\Controller\BaseController;
use App\Service\Assets\LandingLayoutAssets;


class HomeController extends BaseController
{
    public function index()
    {
        return $this->renderLandingPage('home', [
            'assets' => LandingLayoutAssets::getHomeAssets(),
        ]);
    }
}