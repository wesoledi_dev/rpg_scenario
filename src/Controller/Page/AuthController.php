<?php

namespace App\Controller\Page;

use App\Controller\BaseController;
use App\Service\Assets\ClearLayoutAssets;
use App\Service\Auth\AuthService;
use Symfony\Component\HttpFoundation\Request;
use App\Dictionary\AuthError;

class AuthController extends BaseController
{
    private const ERROR_GET_PARAM_KEY = 'er';
    private const DEFAULT_AUTH_ROUTE = 'auth';
    private const SUCCESS_SIGN_IN_ROUTE = 'dashboard';
    private const FAIL_SIGN_IN_ROUTE = self::DEFAULT_AUTH_ROUTE;
    private const SUCCESS_SIGN_OUT_ROUTE = 'home';

    public function index(Request $request)
    {
        return $this->renderClearPage(self::DEFAULT_AUTH_ROUTE, [
            'assets' => ClearLayoutAssets::getAuthAssets(),
        ]);
    }

    public function signOut()
    {
        $service = new AuthService($this->getSession());
        $service->signOut(false);
        return $this->redirectToRoute(self::SUCCESS_SIGN_OUT_ROUTE);
    }
}