<?php

namespace App\Controller\Api;

use App\Controller\BaseController;
use App\Service\Auth\AuthService;
use Symfony\Component\HttpFoundation\Request;
use App\Dictionary\HttpStatus;
use Symfony\Component\HttpFoundation\JsonResponse;

class AuthController extends BaseController
{
    public function signInAction(Request $request): JsonResponse
    {
        $content = json_decode($request->getContent());
        $email = $content->email ?? '';
        $password = $content->pass ?? '';
        
        $service = new AuthService();
        $signed = $service->signIn($email, $password, $this->getDoctrine());

        if (!$signed) {
            return $this->renderApiErrorResponse('Sign in failed!', HttpStatus::HTTP_UNAUTHORIZED);
        }

        return $this->renderApiResponse([]);
    }

    public function registerAction(Request $request): JsonResponse
    {
        $content = json_decode($request->getContent());
        $email = $content->email ?? '';
        $password = $content->pass ?? '';
        $name = $content->name ?? '';

        $service = new AuthService();
        try {
            $registered = $service->register($email, $name, $password, $this->getDoctrine());
            if (!$registered) {
                throw \App\Exception\Auth\RegisterException::saveError();
            }

            /** @TODO: add sending confirmation email */
        } catch (\App\Exception\Auth\RegisterException $e) {
            return $this->renderApiErrorResponse('Sign up failed!', HttpStatus::HTTP_BAD_REQUEST);
        }

        return $this->renderApiResponse([]);
    }
}