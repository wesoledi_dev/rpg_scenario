<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Session\Session;
use App\Dictionary\HttpStatus;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Service\Auth\AuthService;
use App\Entity\App\Auth\AuthUserData;

class BaseController extends AbstractController 
{
    private const LANDING_PATH = 'pages/landing/';
    private const CLEAR_PATH = 'pages/clear/';

    public function getSession(): Session
    {
        $session = new Session();
        return $session;
    }

    public function renderLandingPage(string $pageNamePath, array $params = [])
    {
        $params = array_merge(['user' => $this->acquireUserSignedData()], $params);
        $pagePath = self::LANDING_PATH . $pageNamePath . '.html.twig';
        return $this->render($pagePath, $params);
    }

    public function renderClearPage(string $pageNamePath, array $params = [])
    {
        $pagePath = self::CLEAR_PATH . $pageNamePath . '.html.twig';
        return $this->render($pagePath, $params);
    }

    public function renderApiResponse(array $dataArray, int $status = HttpStatus::HTTP_OK): JsonResponse
    {
        $returnContent = array_merge(self::getBaseMessage($status), ['data' => $dataArray]);
        return new JsonResponse($returnContent, $status);
    }

    public function renderApiErrorResponse(string $message, int $status = HttpStatus::HTTP_BAD_REQUEST): JsonResponse
    {
        $returnContent = array_merge(self::getBaseMessage($status), ['error' => $message]);
        return new JsonResponse($returnContent, $status);
    }

    private static function getBaseMessage(int $status): array
    {
        return [
            'status_code' => $status,
            'status_text' => HttpStatus::getStatusTextValue($status),
        ];
    }

    private function acquireUserSignedData(): array
    {
        $authService = new AuthService();
        $isSigned = $authService->isSigned();
        if ($isSigned) {
            $userData = $authService->getUserData();
        } else {
            $userData = null;
        }

        return $this->buildUserDataParamsPart($isSigned, $userData);
    }

    private function buildUserDataParamsPart(bool $isSigned, ?AuthUserData $userData): array
    {
        return [
            'isSigned' => $isSigned,
            'data' => $userData ?? (new AuthUserData('', '', '')),
        ];
    }
}