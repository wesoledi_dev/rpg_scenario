import Vue from 'vue';
import App from '_vue/pages/auth.vue';

document.addEventListener('DOMContentLoaded', () => {
    new Vue({
        el: '#app',
        data: {},
        render: h => h(App)
    });
});